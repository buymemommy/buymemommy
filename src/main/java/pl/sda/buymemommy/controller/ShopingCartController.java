package pl.sda.buymemommy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.buymemommy.model.AppUser;
import pl.sda.buymemommy.model.AppUserOrder;
import pl.sda.buymemommy.model.ShoppingCart;
import pl.sda.buymemommy.service.AppUserService;
import pl.sda.buymemommy.service.OrderService;
import pl.sda.buymemommy.service.ShoppingCartService;

import java.util.Optional;

@Controller
@RequestMapping(path = "/shoppingCart")
public class ShopingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private OrderService orderService;

    @GetMapping(path = "/shoppingCart")
    public String shopingCart(Model model){
        AppUser loggedInUser = appUserService.getLoggedInUser();
        ShoppingCart loggedInUserShoppingCart = loggedInUser.getShoppingCart();
//        Optional<ShoppingCart> optionalShoppingCart =
//                shoppingCartService.findCartById(loggedInUserShoppingCart.getId());
//        if(optionalShoppingCart.isPresent()){
//            ShoppingCart shoppingCart = optionalShoppingCart.get();
//        }
        model.addAttribute("shoppingCart",loggedInUserShoppingCart);
        return "shoppingCart";
    }

    @GetMapping(path = "/addItemToShoppingCart/{id}")
    public String addItemToShoppingCart(@PathVariable(name = "id") Long id ){
        AppUser loggedInUser = appUserService.getLoggedInUser();
        ShoppingCart shoppingCart = loggedInUser.getShoppingCart();
        shoppingCartService.addItemToShoppingCart(shoppingCart.getId(),id);
        return "redirect:/shoppingCart";
    }

    @GetMapping(path = "removeItemFromShoppingCart/{id}")
    public String removeItemFromShoppingCart(@PathVariable(name = "id")Long id){
        AppUser loggedInUser = appUserService.getLoggedInUser();
        ShoppingCart shoppingCart = loggedInUser.getShoppingCart();
        shoppingCartService.removeItemFromShoppingCart(shoppingCart.getId(),id);
        return "redirect:/shoppingCart";
    }

    @GetMapping(path = "/createOrderFromShoppingCart")
    public String createOrder(Long cartId, Model model) {
        AppUserOrder appUserOrderFromShoppingCart = orderService.createOrderFromShoppingCart(cartId);
        model.addAttribute("orderFromShoppingCart", appUserOrderFromShoppingCart);
        return "appUserOrderFromShoppingCart";//przekierowac na strone podsumowaniem i potwierdzeniem złożonego zamówienia
                                        //redirect??
    }

    /*@GetMapping(path = "/confirmedOrder")
    public String confirmedOrder(Long orderId, Model model){
        AppUserOrder confirmedOrder = orderService.confirmedOrder(orderId);
        model.addAttribute("confirmedOrder", confirmedOrder);
        return "confirmedOrder";
    }*/
}
