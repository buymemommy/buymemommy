package pl.sda.buymemommy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.buymemommy.model.AppUser;
import pl.sda.buymemommy.model.WishList;
import pl.sda.buymemommy.service.AppUserService;
import pl.sda.buymemommy.service.WishListService;

import java.util.Optional;

@Controller
@RequestMapping(path = "/wishList")
public class WishListController {

    @Autowired
    private WishListService wishListService;
    @Autowired
    private AppUserService appUserService;

    @GetMapping(path = "/wishList")
    public String wishList(Model model) {
        AppUser loggedInUser = appUserService.getLoggedInUser();
        WishList loggedInUserWishList = loggedInUser.getWishList();
        Optional<WishList> optionalWishList = wishListService.findListById(loggedInUserWishList.getId());
        if (optionalWishList.isPresent()) {
            WishList wishList = optionalWishList.get();
            model.addAttribute("wishList", wishList);
        }
        return "wishList";
    }

    @GetMapping(path = "/addToWishList/{id}")
    public String addToWishList(@PathVariable(name = "id") Long id) {
        AppUser loggedInUser = appUserService.getLoggedInUser();
        WishList wishList = loggedInUser.getWishList();
        wishListService.addItemToList(wishList.getId(), id);
        return "redirect:/wishList";
    }

    @PostMapping(path = "/removeFromWishList/{id}")
    public String removeFromWishList(@PathVariable(name = "id") Long id) {
        AppUser loggedInUser = appUserService.getLoggedInUser();
        WishList wishList = loggedInUser.getWishList();
        wishListService.removeItemFromList(wishList.getId(), id);
        return "redirect:/wishList";
    }
}