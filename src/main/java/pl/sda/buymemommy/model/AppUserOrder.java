package pl.sda.buymemommy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
public class AppUserOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    private List<Item> items;

    @OneToOne
    private AppUser buyingUser;

    @OneToMany
    private List<AppUser> sellingUsers;

    public AppUserOrder() {
        items = new ArrayList<>();
    }
}
