package pl.sda.buymemommy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.buymemommy.model.Item;
import pl.sda.buymemommy.model.WishList;
import pl.sda.buymemommy.repository.WishListRepository;

import java.util.Optional;

@Service
public class WishListService {

    @Autowired
    private WishListRepository wishListRepository;
    @Autowired
    private ItemService itemService;

    public void add(WishList wishList){
        wishListRepository.save(wishList);
    }

    public Optional<WishList> findListById(Long id){
        return wishListRepository.findById(id);
    }

    public void removeItemFromList(Long listId, Long itemId){
        Optional<WishList> optionalWishList = findListById(listId);
        if(optionalWishList.isPresent()){
            Optional<Item> optionalItem = itemService.find(itemId);
            if(optionalItem.isPresent()){
                WishList list = optionalWishList.get();
                list.getItems().remove(optionalItem.get());
                wishListRepository.save(list);
            }
        }
    }

    public void addItemToList(Long listId, Long itemId){
        Optional<WishList> optionalWishList = findListById(listId);
        if(optionalWishList.isPresent()){
            Optional<Item> optionalItem = itemService.find(itemId);
            if (optionalItem.isPresent()) {
                WishList list = optionalWishList.get();
                list.getItems().add(optionalItem.get());
                wishListRepository.save(list);
            }
        }
    }
}
