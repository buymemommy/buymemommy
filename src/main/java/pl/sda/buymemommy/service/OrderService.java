package pl.sda.buymemommy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.buymemommy.model.AppUser;
import pl.sda.buymemommy.model.Item;
import pl.sda.buymemommy.model.AppUserOrder;
import pl.sda.buymemommy.model.ShoppingCart;
import pl.sda.buymemommy.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private AppUserService appUserService;



    public Optional<AppUserOrder> findOrderById(Long orderId){
       return orderRepository.findById(orderId);
    }
    public AppUserOrder createOrderFromShoppingCart(Long cartId){
        Optional<ShoppingCart> optionalShoppingCart = shoppingCartService.findCartById(cartId);
        if(optionalShoppingCart.isPresent()){
            ShoppingCart shoppingCart = optionalShoppingCart.get();
            AppUserOrder appUserOrder = new AppUserOrder();
            for (Item item:shoppingCart.getItems()) {
                appUserOrder.getItems().add(item);
            }
            AppUser loggedInUser = appUserService.getLoggedInUser();
            appUserOrder.setBuyingUser(loggedInUser);
            List<AppUser> sellingUsers = new ArrayList<>();
            for (Item item: appUserOrder.getItems()) {
//                    sellingUsers.addAll(item.getSellingUser);
            }
            appUserOrder.setSellingUsers(sellingUsers);
            AppUserOrder savedAppUserOrder = orderRepository.save(appUserOrder);
            return savedAppUserOrder;
        }
        return null;
    }


    /*public AppUserOrder confirmedOrder(Long orderId) {
        Optional<AppUserOrder> optionalOrder = orderRepository.findById(orderId);
        if(optionalOrder.isPresent()){

        }
    }*/
}
