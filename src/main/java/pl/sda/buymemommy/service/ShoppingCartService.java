package pl.sda.buymemommy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.buymemommy.model.Item;
import pl.sda.buymemommy.model.ShoppingCart;
import pl.sda.buymemommy.repository.OrderRepository;
import pl.sda.buymemommy.repository.ShoppingCartRepository;

import java.util.Optional;

@Service
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ItemService itemService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;



    public void addShoppingCart(ShoppingCart shoppingCart){
        shoppingCartRepository.save(shoppingCart);
    }

    public Optional<ShoppingCart> findCartById(Long id){
       return shoppingCartRepository.findById(id);
    }

    public void addItemToShoppingCart(Long cartId ,Long itemId){
        Optional<ShoppingCart>optionalShoppingCart = findCartById(cartId);
        if(optionalShoppingCart.isPresent()){
            Optional<Item>optionalItem = itemService.find(itemId);
            if(optionalItem.isPresent()){
                ShoppingCart shoppingCart = optionalShoppingCart.get();
                shoppingCart.getItems().add(optionalItem.get());
                shoppingCartRepository.save(shoppingCart);
            }
        }
    }

    public void removeItemFromShoppingCart(Long cartId, Long itemId){
        Optional<ShoppingCart>optionalShoppingCart = findCartById(cartId);
        if (optionalShoppingCart.isPresent()){
            Optional<Item>optionalItem = itemService.find(itemId);
            if(optionalItem.isPresent()){
                ShoppingCart shoppingCart = optionalShoppingCart.get();
                shoppingCart.getItems().remove(optionalItem.get());
                shoppingCartRepository.save(shoppingCart);
            }
        }
    }


}
