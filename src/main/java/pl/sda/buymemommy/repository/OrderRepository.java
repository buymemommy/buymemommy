package pl.sda.buymemommy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.buymemommy.model.AppUserOrder;

@Repository
public interface OrderRepository extends JpaRepository<AppUserOrder,Long> {
}
